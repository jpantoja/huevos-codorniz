package com.hdc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hdc.entity.PointSale;


public interface PointSaleRepository extends JpaRepository<PointSale, Long> {

	
	
	
}
