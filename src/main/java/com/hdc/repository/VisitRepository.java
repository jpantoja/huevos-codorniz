package com.hdc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hdc.entity.Visit;


public interface VisitRepository extends JpaRepository<Visit, Long> {

	
	
	
}
